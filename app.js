const express = require("express");
const app = express();
const userRouter = require("./route");
const cors = require("cors");
const swaggerUI = require("swagger-ui-express");
const swagger = require("./swagger.json");

app.use(cors());

app.use(express.json());

app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile("index.html");
});

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swagger));

app.use("/", userRouter);

app.listen(3030, function () {
  console.log("server ok");
});
