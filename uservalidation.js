const { body } = require("express-validator");

const registrationValidation = [
  body("username")
    .notEmpty()
    .withMessage("Please enter the username")
    .isString()
    .withMessage("Must be character"),
  body("email")
    .notEmpty()
    .withMessage("Please enter the password")
    .isEmail()
    .withMessage("Must be correct email"),
  body("paassword")
    .notEmpty()
    .withMessage("Please enter the password")
    .isLength({ min: 8, max: 15 })
    .withMessage("Password Min 8 Max 15 characters"),
];
const loginValidation = [
  body("userOrEmail")
    .notEmpty()
    .withMessage("Please enter the username")
    .isString()
    .withMessage("Must be character"),
  body("paassword")
    .notEmpty()
    .withMessage("Please enter the password")
    .isLength({ min: 8, max: 15 })
    .withMessage("Password Min 8 Max 15 characters"),
];
const updateBioValidation = [
  body("phoneNumber").notEmpty().withMessage("Please enter the phone number"),
  body("address")
      .notEmpty()
      .withMessage("Please enter the address")
      .isString()
      .withMessage("Must be character"),
  body("fullname")
      .notEmpty()
      .withMessage("Please enter the fullname")
      .isString()
      .withMessage("Must be character"),
];
const gameHistory = [
  body("resultGame").notEmpty().withMessage("Please enter the history"),
  body("gameName").isString().withMessage("Must be character"),
];

const createRoom = [
  body("namaRoom").notEmpty().withMessage("Tolong masukan nama room"),
  body("playerOneChoice")
    .notEmpty()
    .withMessage("Tolong masukan pilihan kamu")
    .isString()
    .withMessage("Format salah, harus menggunakan huruf"),
];

const joinRoom = [
  body("playerTwoChoice")
    .notEmpty()
    .withMessage("Tolong masukan pilihan kamu")
    .isString()
    .withMessage("Format salah, harus menggunakan huruf"),
];

const vsCom = [
  body("player1_choice")
    .notEmpty()
    .withMessage("Tolong masukan pilihanmu batu/kertas/gunting")
    .isString()
    .withMessage("Format salah, harus berupa Huruf/Character"),
];


module.exports = {
  registrationValidation,
  loginValidation,
  updateBioValidation,
  gameHistory,
  createRoom,
  joinRoom,
  vsCom
};
