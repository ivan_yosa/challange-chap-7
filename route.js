const express = require("express");
const userRouter = express.Router();
const userController = require("./controller");
const authMiddleware = require("./middleware/authMiddleware");
const userValidation = require("./uservalidation");
const midValidationSchema = require("./middleware/midValidationSchema");
const checkValidation = require("./middleware/authProtection");

userRouter.get("/user", authMiddleware, userController.getAllUser);

// register dan login tidak diberikan proteksi | schema melakukan pengecekan
userRouter.post(
  "/register",
  userValidation.registrationValidation,
  midValidationSchema.validationSchema,
  userController.registerNewUser
);

userRouter.post(
  "/login",
  userValidation.registrationValidation,
  midValidationSchema.validationSchema,
  userController.loginUser
);

userRouter.post("/registerBio", userController.registerNewUserBio);

userRouter.get("/userBio/:userId", authMiddleware, userController.findUserBio);

userRouter.put(
  "/updateUserBio/:userId",
  authMiddleware,
  checkValidation,
  userValidation.updateBioValidation,
  midValidationSchema.validationSchema,
  userController.updateUserBio
);

userRouter.put("/inserthistory", userController.updateHistory);

userRouter.get(
  "/gamehistory/:userId",
  authMiddleware,
  userController.gameHistory
);

//  MEMBUAT GAME

// API create room untuk player 1

userRouter.post(
  "/createRoom",
  authMiddleware,
  userValidation.createRoom,
  midValidationSchema.validationSchema,
  userController.createRoom
);

// API CREATE ROOM BY PLAYER 2

userRouter.put(
  "/joinRoom/:id",
  authMiddleware,
  userValidation.joinRoom,
  midValidationSchema.validationSchema,
  userController.joinRoom
);

// API UNTUK MENDAPATKAN SEMUA ROOM / LOBBY

userRouter.get("/allRooms", authMiddleware, userController.getAllRoom);

// API UNTUK MENDAPATKAN INFO DARI SEBUAH ROOM

userRouter.get("/singleRoom/:id", authMiddleware, userController.getSingleRoom);

// API UNTUK MEMANGGIL HISTORY DARI SETIAP USER BERDASARKAN ID DARI TOKEN.ID

userRouter.get("/pvpHistory", authMiddleware, userController.singleUserHistory);

// API create room vs Com dan menambahkan di message agar player mengetahui hasil permainan melawan com
// jadikan player2_ID = Vs COM

userRouter.post(
  "/vsCom",
  authMiddleware,
  userValidation.vsCom,
  midValidationSchema.validationSchema,
  userController.playerVsCom
);


module.exports = userRouter;
