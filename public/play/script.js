        let userPilih = "";
        let comPilih = "";

        const rockBox = document.getElementById('rock');
        const paperBox = document.getElementById('paper');
        const scissorBox = document.getElementById('scissor');
        const reset = document.getElementById('reset');
        const rockBoxCom = document.getElementById('rockCom');
        const paperBoxCom = document.getElementById('paperCom');
        const scissorBoxCom = document.getElementById('scissorCom');

        const VS = document.getElementById('pembungkusvs');        
        const winSection = document.getElementById('playerWin');
        const loseSection = document.getElementById('comWin');
        const drawSection = document.getElementById("draw");

        // const userchoice = document.querySelectorAll('.pilihanPlayer');
        // const comChoice = document.querySelectorAll('.pilihanCom');
        // console.log(userChoice);

        class Computer {
             bgCom = (selected) => {
                const selectedBox = document.getElementById(selected);
                selectedBox.style.backgroundColor = "grey";
                selectedBox.style.borderRadius ='20px';
            }
    
             comMilih = () => {
                const pilihannya = ['rockCom', 'paperCom', 'scissorCom'];
                const pilRnd = Math.floor(Math.random() * 3);
                const pilCom = pilihannya[pilRnd];
                this.bgCom(pilCom);
                comPilih = pilCom;
                
            }
    
        }
        const namaHasil = ['DRAW', 'YOU WIN','YOU LOSE']
        for (let j = 0; j < namaHasil.length; j++) {
            console.log(namaHasil[j])

        }

        const com = new Computer();
        
        const hasil = () => {
            if (userPilih === 'rock' && comPilih === 'rockCom') {
                VS.style.display ="none";
                drawSection.style.display = "";
                drawSection.style.backgroundColor = "green";
                drawSection.style.color ="white";
                drawSection.style.borderRadius ="20px";
                drawSection.style.transform = "rotate(330deg)";
                drawSection.innerText = "DRAW"
                drawSection.style.marginTop ="140px";
                drawSection.style.marginBottom = "120px";
                console.log("DRAW"); 
            } else if (userPilih === 'paper' && comPilih === 'paperCom') {
                VS.style.display ="none";
                drawSection.style.display = "";
                drawSection.style.backgroundColor = "green";
                drawSection.style.color ="white";
                drawSection.style.borderRadius ="20px";
                drawSection.style.transform = "rotate(330deg)";
                drawSection.innerText = "DRAW"
                drawSection.style.marginTop ="140px";
                drawSection.style.marginBottom = "120px";
                console.log("DRAW");
                
            } else if (userPilih === 'scissor' && comPilih === 'scissorCom') {
                VS.style.display ="none";
                drawSection.style.display = "";
                drawSection.style.backgroundColor = "green";
                drawSection.style.color ="white";
                drawSection.style.borderRadius ="20px";
                drawSection.style.transform = "rotate(330deg)";
                drawSection.innerText = "DRAW"
                drawSection.style.marginTop ="140px";
                drawSection.style.marginBottom = "120px";
                console.log("DRAW");
            } else if (userPilih === 'rock' && comPilih === 'scissorCom') {
                VS.style.display = "none";
                winSection.style.display = "";
                winSection.style.backgroundColor = "green";
                winSection.style.color ="white";
                winSection.style.borderRadius ="20px";
                winSection.style.transform = "rotate (330deg)";
                winSection.innerText = "YOU WIN";
                winSection.style.marginTop ="140px";
                winSection.style.marginBottom = "120px";
                console.log("YOU WIN");
            } else if (userPilih === 'paper' && comPilih === 'rockCom') {
                VS.style.display = "none";
                winSection.style.display ="";
                winSection.style.backgroundColor = "green";
                winSection.style.color ="white";
                winSection.style.borderRadius ="20px";
                winSection.style.transform = "rotate(330deg)";
                winSection.innerText = "YOU WIN";
                winSection.style.marginTop ="140px";
                winSection.style.marginBottom = "120px";
                console.log("YOU WIN");
            } else if (userPilih === 'scissor' && comPilih === 'paperCom') {
                VS.style.display = "none";
                winSection.style.display ="";
                winSection.style.backgroundColor = "green";
                winSection.style.color ="white";
                winSection.style.borderRadius ="20px";
                winSection.style.transform = "rotate(330deg)";
                winSection.innerText = "YOU WIN";
                winSection.style.marginTop ="140px";
                winSection.style.marginBottom = "120px";
                console.log("YOU WIN");
            } else if (userPilih === 'rock' && comPilih === 'paperCom') {
                VS.style.display ="none";
                loseSection.style.display ="";
                loseSection.style.backgroundColor = "green";
                loseSection.style.color ="white";
                loseSection.style.borderRadius ="20px";
                loseSection.style.transform = "rotate(330deg)";
                loseSection.innerText = "YOU LOSE";
                loseSection.style.marginTop ="140px";
                loseSection.style.marginBottom = "120px";
                console.log("YOU LOSE");
            } else if (userPilih === 'paper' && comPilih === 'scissorCom') {
                VS.style.display ="none";
                loseSection.style.display ="";
                loseSection.style.backgroundColor = "green";
                loseSection.style.color ="white";
                loseSection.style.borderRadius ="20px";
                loseSection.style.transform = "rotate(330deg)";
                loseSection.innerText = "YOU LOSE";
                loseSection.style.marginTop ="140px";
                loseSection.style.marginBottom = "120px";
                console.log("YOU LOSE");
            } else if (userPilih === 'scissor' && comPilih === 'rockCom') {
                VS.style .display ="none"
                loseSection.style.display ="";
                loseSection.style.backgroundColor = "green";
                loseSection.style.color ="white";
                loseSection.style.borderRadius ="20px";
                loseSection.style.transform = "rotate(330deg)";
                loseSection.innerText = "YOU LOSE";
                loseSection.style.marginTop ="140px";
                loseSection.style.marginBottom = "120px";
                console.log("YOU LOSE");
                
            } 
            
            


        }
        let clicked = false; 
        reset.onclick = () => {
            rockBox.style.backgroundColor = '';
            paperBox.style.backgroundColor = '';
            scissorBox.style.backgroundColor = '';
            paperBox.style.pointerEvents = '';
            scissorBox.style.pointerEvents = '';
            rockBox.style.pointerEvents = '';
            rockBoxCom.style.backgroundColor = '';
            paperBoxCom.style.backgroundColor = '';
            scissorBoxCom.style.backgroundColor = '';
            VS.style.display = '';
            winSection.style.display = "none";
            loseSection.style.display = "none";
            drawSection.style.display = "none";
            clicked = '';
        }
        rockBox.onclick = () => {
            if (!clicked) {
            clicked = true;
            rockBox.style.backgroundColor = 'grey';
            rockBox.style.borderRadius ='20px';
            userPilih = 'rock';
            com.comMilih();
            hasil();
            paperBox.style.pointerEvents = 'none';
            scissorBox.style.pointerEvents = 'none';
            }
        }
        paperBox.onclick = () => {
            if (!clicked) {
                clicked = true;
            paperBox.style.backgroundColor = 'grey';
            paperBox.style.borderRadius ='20px';
            userPilih = 'paper'
            com.comMilih();
            hasil();
            scissorBox.style.pointerEvents = 'none';
            rockBox.style.pointerEvents = 'none';
            }
        }

        scissorBox.onclick = () => {
            if (!clicked) {
                clicked = true;
            scissorBox.style.backgroundColor = 'grey'
            scissorBox.style.borderRadius ='20px';
            userPilih = 'scissor';
            com.comMilih();
            hasil();
            rockBox.style.pointerEvents = 'none';
            paperBox.style.pointerEvents = 'none';
            }
        }

        const disableClick =()=> {
            rockBox.style.pointerEvents ='none';
            paperBox.style.pointerEvents = 'none';
            scissorBox.style.pointerEvents = 'none';
        }

        // function reset() {
        // rockBox.style.backgroundColor = 'white'
        // paperBox.style.backgroundColor = 'white'
        // scissorBox.style.backgroundColor = 'white'

        // }

