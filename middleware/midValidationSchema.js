const { validationResult } = require("express-validator");

const validationSchema = async (req, res, next) => {
  const result = await validationResult(req);

  if (result.isEmpty()) {
    next();
  } else {
    res.statusCode = 400;

    return res.json({errors: result.array()});
  }
};

module.exports = {validationSchema};
