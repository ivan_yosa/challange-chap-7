const jwtApp = require("jsonwebtoken");

const authMiddleware = async (req, res, next) => {
    // cek apakah ada authorization didalam header
    // ambil key authorization dalam header
    const { authorization } = req.headers;
    // console.log (authorization);
    
    // jika authorization undefined / null maka reject req
    if (authorization === undefined) {
      res.statusCode = 400;
      return res.json({ message: "Unauthorized" });
    }
    try {
    const splitedToken = authorization.split(" ")[1];
    console.log(splitedToken);
      // cek apakah authorization valid ?, kalau tida valid return unauthorized, jika valid lanjutkan proses
    const token = await jwtApp.verify(
      splitedToken,
      "nR+sdqoZVkTT69BcqB8AeEEBR6OHNUXelx4JrAEgefCePpd9i8zCqlKYqhu/vBwyWYSwrj8NUeJ87PLW3E7ywqwXfJT9TaYVANaco110D1k=",
    );
      req.user= token;
    next();
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "Invalid token" });
    }
};

module.exports = authMiddleware;