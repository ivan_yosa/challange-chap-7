checkValidation = (req, res, next) => {
    const {userId} = req.params;
    const idRequest = req.user.id;

    console.log(userId);
    console.log(idRequest);

    if (idRequest.toString() !== userId) {
        return res.send({message: "Not has Athorization"});
    }

    next();
}

module.exports = checkValidation;