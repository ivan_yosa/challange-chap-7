const { validationResult } = require("express-validator");
const userModel = require("./model.js");
const jwtApp = require("jsonwebtoken");
const judgementRPS = require("./utils/judgementValidation.js");
class UserController {
  getAllUser = async (req, res) => {
    const allUser = await userModel.getAllUser();
    return res.json(allUser);
  };

  // ganti req,res | else if ganti userData
  registerNewUser = async (req, res) => {
    const dataUser = req.body;

    //  ganti user , email, pass, UserData + await
    const sudahMasuk = await userModel.isUserRegistered(dataUser);

    if (sudahMasuk) {
      return res.json({ message: "Username or Email allready exist" });
    }

    // record data user baru
    userModel.recordNewData(dataUser);
    return res.json({ message: "succes add new user" });
  };

  registerNewUserBio = async (req, res) => {
    const UserBio = req.body;
    try {
      if (
        UserBio.fullname == "" &&
        UserBio.phoneNumber == "" &&
        UserBio.address == "" &&
        UserBio.user_id == ""
      ) {
        res.statusCode = 404;
        return res.json({ message: "opps..write your fullname" });
      } else if (UserBio.phoneNumber == "") {
        res.statusCode = 404;
        return res.json({ message: "write your phone number" });
      } else if (UserBio.address == "") {
        res.statusCode = 404;
        return res.jsson({ message: "write your address" });
      } else if (UserBio.user_id == "") {
        res.statusCode = 404;
        return res.json({
          message: "field your user id",
        });
      }
    } catch (error) {
      console.log(error);
    }
    // push git
    // ganti user,email,pass userBio +await
    const bioMasuk = await userModel.isUserBioExist(UserBio);
    if (bioMasuk) {
      res.statusCode = 404;
      return res.json({ message: "data already exist" });
    }

    // params ganti UserBio
    userModel.registerNewUserBio(UserBio);
    return res.json({ message: "Sucess for new user !" });
  };

  loginUser = async (req, res) => {
    const { username, email, paassword } = req.body;
    const sortedUser = await userModel.confirmLogin(username, email, paassword);

    if (sortedUser) {
      // console.log (sortedUser);

      // generate JWT
      const token = jwtApp.sign(
        { ...sortedUser, role: "player" },
        "nR+sdqoZVkTT69BcqB8AeEEBR6OHNUXelx4JrAEgefCePpd9i8zCqlKYqhu/vBwyWYSwrj8NUeJ87PLW3E7ywqwXfJT9TaYVANaco110D1k=",
        { expiresIn: "1d" }
      );
      console.log(token);
      return res.json({ accessToken: token });
    } else {
      res.statusCode = 404;
      return res.json({ message: "your acount is not valid" });
    }
  };

  findUserBio = async (req, res) => {
    const { userId } = req.params;
    console.log(userId);

    try {
      const user = await userModel.findUserBio(userId);

      if (user) {
        return res.json(user);
      } else {
        res.statusCode = 404;
        console.log(userId);
        return res.json({ message: " user id tidak ditemukan: " + userId });
      }
    } catch (error) {
      res.statusCode = 404;
      console.log(userId);
      return res.json({ message: "user id tidak ditemukan :" + userId });
    }
  };

  updateUserBio = async (req, res) => {
    const { userId } = req.params;
    const { fullname, phoneNumber, address } = req.body;

    // cek apakah data sudah ada atau belum
    try {
      const bioMasuk = await userModel.isUserBioExist(userId);
      if (bioMasuk) {
        // jika userId sudah terdaftar update biodata
        userModel.updateUserBio(userId, fullname, phoneNumber, address);
        return res.json({ message: "update user berhasil " });
      } else {
        // jika userId tidak ada di db dibuatkan biodata baru
        const data = await userModel.registerNewUserBio(
          userId,
          fullname,
          phoneNumber,
          address
        );
        return res.json({ message: "Biodata sudah dibuat" });
      }
    } catch (error) {
      console.log(error);
      res.statusCode = 400;
      res.json({ message: "error try catch controller updateUserBio" });
    }
  };

  updateHistory = async (req, res) => {
    const userGame = req.body;

    if (
      userGame.games == "" &&
      userGame.time == "" &&
      userGame.status == "" &&
      userGame.user_id == ""
    ) {
      return res.json({ message: "write your fullname" });
    } else if (userGame.status == "") {
      return res.json({ message: "field your status" });
    } else if (userGame.user_id == "") {
      return res.json({ message: "field your userid" });
    } else if (userGame.games == "") {
      return res.json({ message: "field your game names" });
    }

    userModel.updateHistory(userGame);
    return res.json({ message: "Success new user history" });
  };

  gameHistory = async (req, res) => {
    const { userId } = req.params;
    const gameHistory = await userModel.gameHistory(userId);
    return res.json(gameHistory);
  };

  createRoom = async (req, res) => {
    const player1 = req.body;
    const token = req.user.id;
    console.log(token);

    try {
      await userModel.createRoom(player1, token);
      return res.json({ message: "room sudah dibuat" });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ mesage: "ada salah di controller create room " });
    }
  };

  joinRoom = async (req, res) => {
    const player2 = req.body;
    const token = req.user.id;
    const { id } = req.params;

    try {
      // mencari room berdasarkan ID
      const singleRoom = await userModel.getSingleRoom(id);

      // buat agar player tidak melawan dirinya sendiri
      if (singleRoom.player1_id === token) {
        res.statusCode = 400;
        return res.json({ message: "Dilarang bermain melawan diri sendiri" });
      }

      // membuat agar player tidak dapat bermain atau mengganti data di room yang sudah selesai
      if (singleRoom.status_room === "Finish") {
        res.statusCode = 400;
        return res.json ({ message: "Room sudah selesai" });
      }
      // input data pilihan player 2 ke database
      await userModel.joinRoom(player2, token, id);

      const p1Result = singleRoom.player1_choice;
      const p2Result = singleRoom.player2_choice;

      // menentukan pemenang
      const result = await judgementRPS.judgementRPS(p1Result, p2Result);

      // masukan result pada database
      const roomResult = await userModel.roomResult(result, id);
      res.json({ message: "berhasil melawan "});
      return roomResult
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ message: "ada yang salah di controller joinRoom" });
    }
  };

  getAllRoom = async (req, res) => {
    try {
      const allRooms = await userModel.getAllRoom();
      return res.json(allRooms);
    } catch (error) {
      return res
        .status(500)
        .json({ message: "ada kesalahan di controller getAllRoom" });
    }
  };

  getSingleRoom = async (req, res) => {
    const { id } = req.params;
    try {
      const singleRoom = await userModel.getSingleRoom(id);
      return res.json(singleRoom);
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ message: "ada yang salah di controller getSingleRoom" });
    }
  };

  playerVsCom = async (req, res) => {
    const id = req.token.id;
    const { player1_choice } = req.body;

    try {
      // create room dan rekam pilihan player 1
      const nama_room = "VS COM";
      const player2_choice = judgementRPS.vsCom();
      const [player1_result, player2_result] = judgementRPS.judgementRPS(
        player1_choice,
        player2_choice
      );
      await userModel.playerVsCom(
        nama_room,
        id,
        player1_choice,
        player1_result,
        player2_choice,
        player2_result
      );
      return res.json({ message: "berhasil membuat room playerVsCom" });
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ message: "error di controller playerVsCom" });
    }
  };

  singleUserHistory = async (req, res) => {
    const id = req.user.id;
    try {
      const userhistory = await userModel.singleUserHistory(id);
      return res.json(userhistory);
    } catch (error) {
      return res
        .status(500)
        .json({ message: "ada error dicontroller singleUserHistory" });
    }
  };

  playerVsCom = async (req, res) => {
    const id = req.user.id;
    const { player1_choice } = req.body;

    try {
      // create room dan rekan pilihan player 1
      const nama_room = "VS COM";
      const player2_choice = judgementRPS.vsCom();
      const [player1_result, player2_result] = judgementRPS.judgementRPS(
        player1_choice,
        player2_choice
      );

      // buat agar inputan hanya berupa batu kertas atau gunting
      if (
        player1_choice !== "batu" &&
        player1_choice !== "kertas" &&
        player1_choice !== "gunting"
      ) {
        return res
          .status(400)
          .json({ message: "Format harus berupa batu / kertas / gunting " });
      }

      await userModel.playerVsCom(
        nama_room,
        id,
        player1_choice,
        player1_result,
        player2_choice,
        player2_result
      );
      return res.json({ message: "berhasil membuat room playerVsCom" });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "pilihan harus berupa batu / kertas / gunting" });
    }
  };
}

module.exports = new UserController();
