// model = sumber data

const md5 = require("md5");
const db = require("./db/models");
const { Op } = require("sequelize");
const { Result } = require("express-validator");

let userlist = [];

class UserModel {
  // cek semua user data
  getAllUser = async () => {
    const dataUsers = await db.User.findAll({
      include: [db.UserBio, db.userhistory],
    });
    // select * FROM USERS
    return dataUsers;
  };

  // check username teregister? yes or no / T or F
  isUserRegistered = async (dataUser) => {
    const sudahMasuk = await db.User.findOne({
      where: {
        [Op.or]: [{ username: dataUser.username }, { email: dataUser.email }],
      },
    });

    if (sudahMasuk) {
      return true;
    } else {
      return false;
    }
  };

  isUserBioExist = async (userId) => {
    const bioMasuk = await db.UserBio.findOne({
      where: { user_id: userId },
      // [Op.or]: [
      //   { fullname: UserBio.fullname },
      //   { phoneNumber: UserBio.phoneNumber },
      //   { user_id: UserBio.user_id },
      // ],
    });
    if (bioMasuk) {
      return true;
    } else {
      return false;
    }
  };
  //    method record new data

  recordNewData = (dataUser) => {
    //  INSERT INTO table ()values()
    db.User.create({
      username: dataUser.username,
      email: dataUser.email,
      // // password hashed md5
      paassword: md5(dataUser.paassword),
    });
  };

  confirmLogin = async (username, email, paassword) => {
    const sortedUser = await db.User.findOne({
      where: { username: username, email: email, paassword: md5(paassword) },
      attributes: { exclude: ["paassword"] },
      raw: true,
    });

    return sortedUser;
  };
  findUserBio = async (userId) => {
    console.log(userId);
    return await db.User.findOne({
      include: [db.UserBio],
      where: { id: userId },
    });
  };

  updateUserBio = async (userId, fullname, phoneNumber, address) => {
    return await db.UserBio.update(
      { fullname: fullname, phoneNumber: phoneNumber, address: address },
      { where: { user_id: userId } }
    );
  };

  registerNewUserBio = (userId, fullname, phoneNumber, address) => {
    return db.UserBio.create({
      fullname,
      phoneNumber,
      address,
      user_id: userId,
    });
  };

  updateHistory = (userGame) => {
    db.userhistory.create({
      games: userGame.games,
      time: userGame.time,
      status: userGame.status,
      user_id: userGame.user_id,
    });
  };

  gameHistory = async (userId) => {
    return await db.User.findOne({
      include: [db.userhistory],
      where: { id: userId },
    });
  };

  createRoom = (player1, token) => {
    db.game_room.create({
      nama_room: player1.namaRoom,
      player1_id: token,
      player1_choice: player1.playerOneChoice,
      status_room: "Available",
    });
  };

  joinRoom = (player2, token, id) => {
    const roomId = parseInt(id);
    db.game_room.update(
      {
        player2_choice: player2.playerTwoChoice,
        player2_id: token,
      },
      { where: { id: roomId } }
    );
  };

  getAllRoom = async () => {
    const allRoom = await db.game_room.findAll({
      include: [
        {
          model: db.User,
          as: "player1",
        },
        {
          model: db.User,
          as: "player2",
        },
      ],
    });
    return allRoom;
  };

  getSingleRoom = async (id) => {
    const roomId = parseInt(id);
    const singleRoom = await db.game_room.findOne({
      where: { id: roomId },

      include: [
        {
          model: db.User,
          as: "player1",
        },
        {
          model: db.User,
          as: "player2",
        },
      ],
      raw: true,
    });
    return singleRoom;
  };
  roomResult = async (result, id) => {
    const roomId = parseInt(id);
    await db.game_room.update(
      {
        player1_result: result[0],
        player2_result: result[1],
        status_room: "finish",
      },
      { where: { id: roomId } }
    );
  };
  playerVsCom = async (
    nama_room,
    id,
    player1_choice,
    player1_result,
    player2_choice,
    player2_result
  ) => {
    try {
      return await db.game_room.create({
        nama_room,
        player1_id: id,
        player1_choice,
        player1_result,
        player2_choice,
        player2_result,
        status_room: "Finish",
      });
    } catch (error) {
      console.log(error);
    }
  };

  singleUserHistory = async (id) => {
    try {
      const roomList = await db.game_room.findAll({
        attributes: [
          "nama_room",
          "updatedAt",
          "player1_id",
          "player2_id",
          "player1_result",
          "player2_result",
        ],
        where: {
          [Op.or]: [{ player1_id: id }, { player2_id: id }],
          [Op.and]: [
            { player1_result: { [Op.ne]: null } },
            { player2_result: { [Op.ne]: null } },
          ],
        },
        raw: true,
      });
      console.log(roomList);

      return roomList.map((room) => ({
        player_id: room.player1_id === id ? room.player1_id : room.player2_id,
        hasilPlayer:
          room.player1_id === id ? room.player1_result : room.player2_result,
        roomName: room.nama_room,
        updatedAt: room.updatedAt,
      }));
    } catch (error) {
      throw new Error("Gagal menerima room detail : " + error.message);
    }
  };
}

module.exports = new UserModel();
